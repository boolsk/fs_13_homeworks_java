package Homework_3_java;

import java.util.Scanner;

public class Planner {
    public static Scanner scanner = new Scanner(System.in);
    public static String userDay;

    public static void doCurrentDay() {
        String eneredDay = scanner.nextLine().toUpperCase().trim();
        userDay = (eneredDay.charAt(0) + eneredDay.substring(1).toLowerCase());
    }

    public static void main(String[] args) {
        String[][] scedule = new String[7][2];

        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";

        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses";

        scedule[2][0] = "Tuesday";
        scedule[2][1] = "go to football";

        scedule[3][0] = "Wednesday";
        scedule[3][1] = "go to POOL";

        scedule[4][0] = "Thursday";
        scedule[4][1] = "read the Book";

        scedule[5][0] = "Friday";
        scedule[5][1] = "play DOTA";

        scedule[6][0] = "Saturday";
        scedule[6][1] = "go to Sea";

        boolean exit = false;

        while (!exit){
            System.out.println("\n Please, input the day of the week: ");
            doCurrentDay();
            switch (userDay) {
                case "Sunday":
                    System.out.printf("Your tasks for %s: %s", scedule[0][0], scedule[0][1] );
                    break;
                case "Monday":
                    System.out.printf("Your tasks for %s: %s", scedule[1][0], scedule[1][1] );
                    break;
                case "Tuesday":
                    System.out.printf("Your tasks for %s: %s", scedule[2][0], scedule[0][1] );
                    break;
                case "Wednesday":
                    System.out.printf("Your tasks for %s: %s", scedule[3][0], scedule[0][1] );
                    break;
                case "Thursday":
                    System.out.printf("Your tasks for %s: %s", scedule[4][0], scedule[0][1] );
                    break;
                case "Friday":
                    System.out.printf("Your tasks for %s: %s", scedule[5][0], scedule[0][1] );
                    break;
                case "Saturday":
                    System.out.printf("Your tasks for %s: %s", scedule[6][0], scedule[0][1] );
                    break;
                case "Exit":
                    exit = true;
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
            }

        }

    }

}
