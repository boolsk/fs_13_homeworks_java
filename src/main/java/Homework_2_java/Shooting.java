package Homework_2_java;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;

public class Shooting {
    public static int rndX;
    public static int rndY;
    public static int playerX;
    public static int playerY;
    public static boolean finish = false;

    public static void main(String[] args) {
        System.out.println("All set. Get ready to rumble!");
        randomCompTarget();
        int sizeOfField = 5;
        char[][] gameField = new char[sizeOfField][sizeOfField];
        for (int i = 0; i < gameField.length; i++) {
            Arrays.fill(gameField[i], '-');
        }
        createField(sizeOfField, gameField);
        playerShoot();
        while (!finish) {
            if (rndX == playerX && rndY == playerY) {
                gameField[playerX - 1][playerY - 1] = 'X';
                System.out.println("You have won!");
                createField(sizeOfField, gameField);
                finish = true;
            } else {
                gameField[playerX - 1][playerY - 1] = '*';
                createField(sizeOfField, gameField);
                playerShoot();
            }
        }
    }

    public static void playerShoot() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("введи номер строки Х");
        playerX = scanner.nextInt();

        while (playerX < 0 || playerX > 5 || playerX == 0) {
            System.out.println("номер строки должен быть от 1 до 5. Введите номер строки еще раз!");
            playerX = scanner.nextInt();
        }
        System.out.println("введи номер строки Y");
        playerY = scanner.nextInt();
        while (playerY < 0 || playerY > 5 || playerY == 0) {
            System.out.println("номер строки должен быть от 1 до 5. Введите номер строки еще раз!");
            playerY = scanner.nextInt();
        }
        System.out.println("Стреляем по ячейке " + playerX + "," + playerY);

    }

    public static void randomCompTarget() {
        Random random = new Random();
        rndX = random.nextInt(5) + 1;
        rndY = random.nextInt(5) + 1;
        System.out.println("мишень находится в " + rndX + "," + rndY); //для себя (можно убрать строку)
    }

    public static void createField(int sizeOfField, char[][] gameField) {
        for (int i = 0; i <= sizeOfField; i++) {
            System.out.print(i + " | ");
        }
        System.out.println();
        for (int i = 0; i < sizeOfField; i++) {
            System.out.print(i + 1 + " | ");
            for (int j = 0; j < sizeOfField; j++) {
                System.out.print(gameField[i][j] + " | ");
            }
            System.out.println();
        }
    }
}