package Homework_1_java;

import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        Random random = new Random();
        int randNumber = random.nextInt(101);
        System.out.println("Загадоное число Случайное от 0 до 100 = " + randNumber);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите свое Имя");
        String name = scanner.nextLine();
        System.out.println("Let the game begin!");
        System.out.println("Угадай число от 0 до 100");

        do {
            int playerNum = scanner.nextInt();
            if (randNumber > playerNum) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (randNumber < playerNum) {
                System.out.println("Your number is too big. Please, try again.");
            } else if (randNumber == playerNum) {
                System.out.println("Congratulations, " + name);
                break;
            }
        } while (true);
    }
}
